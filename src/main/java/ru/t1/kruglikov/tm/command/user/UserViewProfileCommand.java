package ru.t1.kruglikov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-view-profile";
    @NotNull private final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        System.out.println("[USER VIEW PROFILE]");
        @NotNull final User user = getAuthService().getUser();
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Role: " + user.getRole());
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
