package ru.t1.kruglikov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.api.service.IAuthService;
import ru.t1.kruglikov.tm.api.service.IProjectService;
import ru.t1.kruglikov.tm.api.service.IProjectTaskService;
import ru.t1.kruglikov.tm.command.AbstractCommand;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kruglikov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return getLocatorService().getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getLocatorService().getProjectTaskService();
    }

    protected IAuthService getAuthService() {
        return getLocatorService().getAuthService();
    }

    @Nullable @Override
    public String getArgument() {
        return null;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

}
